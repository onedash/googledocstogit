header_type extracted_t {
  fields {
    data : 160;
  }
}

header extracted_t extracted;

parser start {
  extract(extracted);
  return ingress;
}

action mod_extracted_extracted(leftshift, rightshift, msk) {
  // modify_field(extracted.dcpy, extracted.data);
  // modify_field(extracted.data, (extracted.data & ~msk) | (((extracted.dcpy << leftshift) >> rightshift) & msk) );
  modify_field(extracted.data, (extracted.data & ~msk) | (((extracted.data << leftshift) >> rightshift) & msk) );
}

action _no_op() {
}

table test {
  actions {
    mod_extracted_extracted;
    _no_op;
  }
}

action set_egr(egress_spec) {
    modify_field(standard_metadata.egress_spec, egress_spec);
}

table fwd {
    reads {
        standard_metadata.ingress_port : exact;
    }
    actions {
        set_egr;
    }
}

control ingress {
  apply(test);
  apply(fwd);
}
