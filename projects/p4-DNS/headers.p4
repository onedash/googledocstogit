header_type ethernet_t {
    fields {
        dstAddr : 48;
        srcAddr : 48;
        etherType : 16;
    }
}

header_type ipv4_t {
    fields {
        version : 4;
        ihl : 4;
        diffserv : 8;
        totalLen : 16;
        identification : 16;
        flags : 3;
        fragOffset : 13;
        ttl : 8;
        protocol : 8;
        hdrChecksum : 16;
        srcAddr : 32;
        dstAddr: 32;
    }
}

header_type dns_t {
    fields {
        id : 16;
        qr : 1; // flag to differentiate between queries and responses
        opcode : 4; // to differentiate between different kind of requests
        aa : 1; // authoritative answer flag
        tf : 1 //truncation flag
        rd : 1 //recursion desired
        ra : 1 //recursion avaialbale
        z : 3 //three reserved bits
        rcode : 4 // response code set by server
        qdcount : 16;
        ancount : 16;
        nscount : 16;
        arcount : 16;
    }
}

header_type questionSection_t {
    fields {
        qname : *; //variable fieid,,,, URL
        qtype : 16;
        qclass : 16;
    }
    //maxlength ?
}

header_type answerSection_t {
    fields {
        name : *; //max can be 32
        recordtype : 16;
        class : 16;
        ttl : 32;
        rdlength : 16;
        rddata : 32; //i am assuming we will mostly be dealing with A type
        //rdata field can be the start field as well
    }
}

// parsing goes like ethernet -> ipv4 -> DNS tags -> DNS query -> DNS answer
//
// we have to branch on one of the tag values

parser start {
    return parse_ethernet;
}
header ethernet_t ethernet;

parser parse_ethernet {
    extract(ethernet);
    return select(latest.etherType) {
        0x0800 : parse_ipv4;
        default: ingress;
    }
}

header ipv4_t ipv4;

parser parse_ipv4 {
    extract(ipv4);
    return ingress;
}

header dns_t dns;

parser parser_dns {
    extract(dns);
    return select(latest.qr) {
        1 : parse_response;
        0 : parse_query;
    }
}
header questionSection_t query;
header answerSection_t response;

parser parse_query {
    extract(query);
    return ingress;
}

parser parse_response {
    extract(response);
    return ingress;
}

field_list dnsQueryHash_list {
        dns.qname;
}

field_list_calculation dnsQueryHash {
    input {
        dnsQueryHash_list
    }
    algorithm : crc32;
    output_width : 32;
}



// // Assume standard metadata from compiler.
// // Define local metadata here.
// //
// // copy_to_cpu is an example of target specific intrinsic metadata
// // It has special significance to the target resulting in a
// // copy of the packet being forwarded to the management CPU.
// header_type local_metadata_t {
//     fields {
// cpu_code        : 16; // Code for packet going to CPU
// port_type       : 4;  // Type of port: up, down, local...
// ingress_error   : 1;  // An error in ingress port check
// was_mtagged
// copy_to_cpu
// bad_packet
// color: 1;  // Track if pkt was mtagged on ingr
// : 1;  // Special code resulting in copy to CPU
// : 1;  // Other error indication
// : 8;  // For metering
//   73
//

header_type local_metadata_t {
    fields {

    }
}









//declare registers
// registers are in c++ just a simple array
//  register_reference ::=
// register_name "[" const_value "]" [.field_name]
#define CACHE_SIZE 64000000;
// indexed by the hash
// we also need to define a parallel register storing the url
register dnsCache {
    width : 32;
    instance_count : CACHE_SIZE;
}
//form actions
// not sure if we will be allowed to form this on a variable field
register associatedUrl {
    width : *;
    instance_count : CACHE_SIZE;
}

//assuming that the register cells will be initialized to some default value

action addr_1earner(registerIndex) {
    register_write(dnsCache, registerIndex, response.rddata);
    modify_field();
}

// todo actions

// todo tables

// Check if the packet needs an mtag and add one if it does.
// table mTag_table {
// reads {
//     ethernet.dst_addr
//     vlan.vid
// }
// actions {
// : exact;
// : exact;
// add_mTag;     // Action called if pkt needs an mtag.
//         common_copy_pkt_to_cpu; // If no mtag, send to the CPU
//         no_op;
// }
//     max_size                 : 20000;
// }

// forget about the controller for now
// aim is to parse a packet correctly and then cache it to the register
// step 2 retrieve that packet in from the caceh
