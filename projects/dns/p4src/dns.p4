header_type ethernet_t {
    fields {
        dstAddr : 48;
        srcAddr : 48;
        etherType : 16;
    }
}

header_type ipv4_t {
    fields {
        version : 4;
        ihl : 4;
        diffserv : 8;
        totalLen : 16;
        identification : 16;
        flags : 3;
        fragOffset : 13;
        ttl : 8;
        protocol : 8;
        hdrChecksum : 16;
        srcAddr : 32;
        dstAddr: 32;
    }
}

header_type udp_t {
  fields {
    src_port : 16;
    dst_port : 16;
    len : 16;
    checksum : 16;
  } // 8 B / 64 b
}

header_type dns_t {
    fields {
        id : 16;
        qr : 1; // flag to differentiate between queries and responses
        opcode : 4; // to differentiate between different kind of requests
        aa : 1; // authoritative answer flag
        tf : 1; //truncation flag
        rd : 1; //recursion desired
        ra : 1; //recursion avaialbale
        z : 3;//three reserved bits
        rcode : 4; // response code set by server
        qdcount : 16;
        ancount : 16;
        nscount : 16;
        arcount : 16;
    }
}

header_type questionSection_t {
    fields {
        qtype : 16;
        qclass : 16;
    }
}

header_type answerSection_t {
    fields {
        //name : 8; //max can be 32
        recordtype : 16;
        class : 16;
        ttl : 32;
        rdlength : 16;
        rddata : 32;
    //i am assuming we will mostly be dealing with A type
    //rdata field can be the start field as well
    }
    // length : 340;
    // max_length : 340;
}

header_type charHeader_t {
    fields {
        char : 8;
    }
}

header_type charRHeader_t {
    fields {
        char : 8;
    }
}



header_type testHeader_t {
    fields {
        char : 184;
    }
}

// parsing goes like ethernet -> ipv4 -> DNS tags -> DNS query -> DNS answer

// we have to branch on one of the tag values

header ethernet_t ethernet;
header udp_t udp;
header charHeader_t charHeader[50];
header charRHeader_t charRHeader[50];

header testHeader_t testHeader;

parser start {
    extract(ethernet);
    return select(latest.etherType) {
        0x0800 : parse_ipv4;
        default : ingress;
    }
}

header ipv4_t ipv4;

parser parse_ipv4 {
    extract(ipv4);
    return select(latest.protocol) {
         0x11 : parse_udp;
         default : ingress;
    }
}

header dns_t dns;

parser parse_udp {
    extract(udp);
    return select(latest.dst_port) {
        53 : parse_dns;
        // 0x35: parse_dns;
        default : ingress;
    }
}

parser parse_char {
   extract(charHeader[next]);
   return select(latest.char) {
         0x00 : parse_query;  // Once I hit this case I Know that i have parsed the domain name correctly
         default :parse_char;     //  I am expecting this to recurse back to parse_char and populate the next field what you think?
        //default : parse_query
    }
   //return parse_query;
}


parser parse_dns {
    extract(dns);
    return select(latest.opcode) {
        // 0x0 : parse_response;
        default : parse_char;
        // default : ingress;
    }
}



header questionSection_t query;
header answerSection_t response;

parser parse_query {
    extract(query);
    //return parse_response;
    return parse_rchar;
    //return ingress;
}


parser parse_rchar {
   extract(charRHeader[next]);
   return select(latest.char) {
         0x00 : parse_response;  // Once I hit this case I Know that i have parsed the domain name correctly
         default :parse_rchar;     //  I am expecting this to recurse back to parse_char and populate the next field what you think?
        //default : parse_query
    }
   //return parse_query;
}


parser parse_response {
    extract(response);
    return ingress;
}

field_list dnsQueryHash_list {
    charHeader[0];
    charHeader[1];
    charHeader[2];
    charHeader[4];
    charHeader[5];
    charHeader[6];

}

field_list_calculation dnsQueryHash {
    input {
        dnsQueryHash_list;
    }
    algorithm : crc16;
    output_width : 16;
}

// ipv4 -> udp -> dnsheader -> (headerstack to get bytes) ->
// length of the thing using ipv4.length
// headesstack parsing

// // Assume standard metadata from compiler.
// // Define local metadata here.
// //
// // copy_to_cpu is an example of target specific intrinsic metadata
// // It has special significance to the target resulting in a
// // copy of the packet being forwarded to the management CPU.
// header_type local_metadata_t {
//     fields {
// cpu_code        : 16; // Code for packet going to CPU
// port_type       : 4;  // Type of port: up, down, local...
// ingress_error   : 1;  // An error in ingress port check
// was_mtagged
// copy_to_cpu
// bad_packet
// color: 1;  // Track if pkt was mtagged on ingr
// : 1;  // Special code resulting in copy to CPU
// : 1;  // Other error indication
// : 8;  // For metering
// : 7;
//

header_type local_metadata_t {
    fields {
        tempIp : 32;
        tempField : 16;
    }
}


metadata local_metadata_t local_metadata;

//declare registers
// registers are in c++ just a simple array
//  register_reference ::=
// register_name "[" const_value "]" [.field_name]
#define CACHE_SIZE 64000000;
// indexed by the hash
// we also need to define a parallel register storing the url
register dnsCache {
    width : 32;
    instance_count : 64000;
}
//form actions
// not sure if we will be allowed to form this on a variable field
register associatedUrl {
    width : 32;
    instance_count : 64;
}

//assuming that the register cells will be initialized to some default value

action addr_learner() {
    // set_field_to_hash_index();
    modify_field_with_hash_based_offset(local_metadata.tempField, 2, dnsQueryHash, 21); //index generator
    register_write(dnsCache, local_metadata.tempField, ipv4.dstAddr);
    register_read(local_metadata.tempIp, dnsCache, local_metadata.tempField);
    modify_field(ipv4.srcAddr, local_metadata.tempIp);
    modify_field(response.rddata, local_metadata.tempIp);
    // modify_field(query.qtype, charHeader[]);
    modify_field(response.ttl, local_metadata.tempField);


}

action response_header() {
    add_header(charRHeader);
    add_header(response);
}

action set_egr(egress_spec) {
    modify_field(standard_metadata.egress_spec, egress_spec);
}

table fwd {
    reads {
        standard_metadata.ingress_port : exact;
    }
    actions {
        set_egr;
    }
}
action ipv4_present() {
}

action _drop() {
    drop();
}

table check_ipv4 {
    reads {
        response : valid;
    }
    actions {
        ipv4_present;
        addr_learner;
        _drop;
    }
}

control ingress {
    apply(check_ipv4) {
        addr_learner {
            apply(fwd);
        }
    }
}







// todo actions

// todo tables

// table for cache retrieval


// Check if the packet needs an mtag and add one if it does.
// table mTag_table {
// reads {
//     ethernet.dst_addr
//     vlan.vid
// }
// actions {
// : exact;
// : exact;
// add_mTag;     // Action called if pkt needs an mtag.
//         common_copy_pkt_to_cpu; // If no mtag, send to the CPU
//         no_op;
// }
//     max_size                 : 20000;
// }

// forget about the controller for now
// aim is to parse a packet correctly and then cache it to the register
// step 2 retrieve that packet in from the caceh
